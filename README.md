# Contact Emails

This module provides a more versatile interface and functionality for managing
emails that get sent from Drupal Core Contact submissions. It allows users with
the new permission 'manage contact form emails' to add as many emails as
desired, each with a different recipient or set of recipients (including the
submitter of the form), each a different subject or message.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/contact_emails).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/contact_emails).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Admin Toolbar](https://www.drupal.org/project/admin_toolbar)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

There is nothing to configure. Enable the module and users with the required
permission can find the new 'Manage Emails' section.


## Maintainers

- Scott Euser [scott_euser](https://www.drupal.org/u/scott_euser)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
